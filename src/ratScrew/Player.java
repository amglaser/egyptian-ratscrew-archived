package ratScrew;
import java.util.Collection;
import java.util.Collections;
import java.util.List;


public class Player extends Thread {
	private int _ID; // start with ID #0
	private CardPile _pile;
	private List<String> _cards;
	private boolean _gameStarted;
	private double _startupDelay;
	private boolean _has_cards;
	private double _vel;

	
	Player(int id, List<String> cards, CardPile cardsToTake) {
	 	_ID = id;
	 	_cards = cards;
	 	_pile = cardsToTake;
	 	_startupDelay = 2;
	 	_gameStarted = false;
	 	_has_cards = true;
	 	_vel = 0.001;//0.5; //use 0.001 to create concurrency problems for lab 5 //Factor that multiplies the sleeps parameters
	}
	
	
	
	int get_id() {
	 	return _ID;
	}
	List<String> cards() {
	 	return _cards;
	}
	double num_cards() {
	 	return _cards.size();
	}
	
	void startGame() {
		if (_gameStarted == false){
			System.out.println("Player " + _ID + ": Weeee!");
			Simulation.tryToSleep(_startupDelay, 0);
			_gameStarted = true;
		} else {
			System.out.println("Player " + _ID  + ":OUCH!");
		}
	}
	
	void add_card(String elt){
			_cards.add(elt);	
	}
	
	//public boolean get_pass() {
	//	return _slap_pass.tryAcquire(1);
	//}
	
	//public void drop_pass() {
	//	_slap_pass.release();
	//}
	 
	public void run() {
		
		while (_pile.game_status() == true){
			if (_gameStarted == false){
				startGame();
			}
			
			//Note!!! I decreased all the sleeps by a factor of ten to speed up the pace of the game!
			Simulation.tryToSleep(_vel*.1, 0); //Delay to check pile
			if (_pile.can_Slap() == true){
				Simulation.tryToSleep(_vel*1, _vel*1); //Delay with variation so we don't know who will slap the deck first
				_pile.take_cards(this);
				System.out.println("Player " + _ID + "'s current hand: " + _cards); //Maybe remove this later
			}
			
			
			
			if (_cards.size() == 0){
				_has_cards = false;
			} else {
				_has_cards = true;
			}
			
			
			if (_pile.check_turn() == _ID && _pile.cards_owed() > 0 && _pile.game_status()){
				Simulation.tryToSleep(_vel*2, 0); //Delay so there's always a chance to slap
				if (_has_cards == true){
					_pile.place_card(this, _cards.get(0), _has_cards);
					_cards.remove(0);
				} else {
					_pile.place_card(this, "" , _has_cards);
				}
			}
			
			if (_pile.face_status() && _pile.face_id() == _ID && _pile.cards_owed() == 0){
				_pile.take_cards(this);
			}
			
			//System.out.println("Player " + _ID + ":" + _cards.toString());	
		}
		if (_cards.size() > 0 ){
			System.out.println("Player " + _ID + ": Yay! I won!");	
			System.out.println("Player " + _ID + ":" + _cards.toString());
			Collections.sort(_cards.subList(0, _cards.size()));
			System.out.println("Player " + _ID + ":" + _cards.toString());	
			System.out.println("Player " + _ID + ": I have " + Integer.toString(_cards.size()) + " cards.");	
		}
	}
}
