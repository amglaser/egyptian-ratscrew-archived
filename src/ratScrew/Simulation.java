package ratScrew;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.Collections;


public class Simulation {
	public static void main(String[] args) {
		runConcurrentExample();
	}
	
    private static double minStartingInterval = 0.5; // in seconds; should be at least 0.1
    private static double startingIntervalVar = 2.0; // in seconds, how much more than min interval might occur?
 
	
	public static void runConcurrentExample() {
		System.out.println("== Starting game of Egyptian Rat Screw! =="); 
		int N = 4; //Number of players
		List<String> FullDeck = new ArrayList<String>(Arrays.asList( "2c", "3c", "4c", "5c", "6c", "7c", "8c", "9c", "10c", "Jc", "Qc", "Kc", "Ac", "2d", "3d", "4d", "5d", "6d", "7d", "8d", "9d", "10d", "Jd", "Qd", "Kd", "Ad", "2s", "3s", "4s", "5s", "6s", "7s", "8s", "9s", "10s", "Js", "Qs", "Ks", "As", "2h", "3h", "4h", "5h", "6h", "7h", "8h", "9h", "10h", "Jh", "Qh", "Kh", "Ah"));// obtained from http://stackoverflow.com/questions/13395114/how-to-initialize-liststring-object-in-java
//		List<String> FullDeck = new ArrayList<String>(Arrays.asList( "Kc", "Qc", "2c", "3c", "2d", "7c", "2h", "9c")); // , "10c", "Jc", "Qc", "Kc", "Ac", "2d", "3d", "4d", "5d", "6d", "7d", "8d", "9d", "10d", "Jd", "Qd", "Kd", "Ad", "2s", "3s", "4s", "5s", "6s", "7s", "8s", "9s", "10s", "Js", "Qs", "Ks", "As", "2h", "3h", "4h", "5h", "6h", "7h", "8h", "9h", "10h", "Jh", "Qh", "Kh", "Ah"));// obtained from http://stackoverflow.com/questions/13395114/how-to-initialize-liststring-object-in-java
		Collections.shuffle(FullDeck); // from http://www.vogella.com/tutorials/JavaAlgorithmsShuffle/article.html
		int c = FullDeck.size() % N;
		int d = (FullDeck.size()/N);
		java.util.concurrent.Semaphore slap_pass;
		slap_pass = new java.util.concurrent.Semaphore(N);
		CardPile pile = new CardPile(N, FullDeck.subList(0, c), slap_pass);
		List<Player> players = new ArrayList<Player>();
		for (int i = 0; i < N; i++){
			players.add(new Player(i, new ArrayList<String>(FullDeck.subList(c + i*d, c + (i+1)*d)), pile));
		}
		
		
		for (int i = 0; i < N; i++){
			players.get(i).start();
		}
		
		
		for (int i = 0; i < N; i++){
			try {
				players.get(i).join();  // wait for each player to be done
			} catch  (Exception e) {
	            System.out.println("Not Handling exceptions yet ... goodbye");
			}
		}
		
		
	
	// tryToSleep will pause be at least secMin seconds,
	//     and try not to pause more than secMin + secVar,
	//     but of course extra delay could happen anywhere at any time,
	//     and if something goes wrong (e.g., an interrupt) it could wake early

	private static java.util.Random dice = new java.util.Random(); // random number generator, for delays mostly	
	public static void tryToSleep(double secMin, double secVar) {
        try {
            java.lang.Thread.sleep(Math.round(secMin*1000) + Math.round(dice.nextDouble()*(secVar)*1000));
        } catch (InterruptedException e) {
            System.out.println("Not Handling interruptions yet ... ");
        }
	}
}
