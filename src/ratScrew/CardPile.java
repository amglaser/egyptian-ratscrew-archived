package ratScrew;
import java.util.List;
import java.util.Arrays;

public class CardPile {
	private List<String> _cardsPile;
	private int  _numPlayers;
	private double  _turnID = 0;
	private double _cards_owed = 1;
	private double _consecutive_passes = 0; //how many consecutive players were skipped because they had no cards
	private double _face_id = -1;
	private boolean _face_case = false; //handles face card conditions
	private boolean _gameOver = false;
	private java.util.concurrent.Semaphore _slap_pass;
	CardPile(int numPlayers, List<String> cards, java.util.concurrent.Semaphore pass) {
		_cardsPile = cards; //extra cards are left in the pile for fair play during initialization
		_numPlayers = numPlayers;
		_slap_pass = pass;
	}
	
	
	
	public boolean  game_status() {
		return (! _gameOver);
	}
	
	public void  next_player() {
		_turnID += 1;
		_turnID = _turnID % _numPlayers;
	}
	
	public boolean  face_status() {
		return _face_case;
	}
	
	public void  end_face_case() {
		_face_case = false;
		_face_id = -1;
		_cards_owed = 1;
		next_player(); //do I need a "this." in front of the method??? 
	}
	
	public double  pile_size() {
		return _cardsPile.size();
	}
	
	public double  check_turn() {
		return _turnID;
	}
	
	public double  face_id() {
		return _face_id;
	}
	
	public double  cards_owed() {
		return _cards_owed;
	}
	
	
	//Well this is an annoying problem with Java, you can't use == signs on substrings to see if they agree, because really it's checking if they have the same address in memory
	//source: http://stackoverflow.com/questions/10759453/java-substring-equals-versus
	public boolean  can_Slap() {
		if(_slap_pass.tryAcquire(1)){
			int N = _cardsPile.size();
			if (_cardsPile.size() == 0) {
				_slap_pass.release(1);
				return false;
			} else if ( (N > 1 && _cardsPile.get(N-1).substring(0, _cardsPile.get(N-1).length() -1).equals( _cardsPile.get(N-2).substring(0, _cardsPile.get(N-2).length() -1)) ) || ( N > 2 && _cardsPile.get(N-1).substring(0, _cardsPile.get(N-1).length() -1).equals( _cardsPile.get(N-3).substring(0, _cardsPile.get(N-3).length() -1) ))){
				System.out.println( "!!!" );
				_slap_pass.release(1);
				return true;
			} else {
				_slap_pass.release(1);
				return false;
			} 
		} else {
			return false;
		}
	}
	
	void decrement_cards_owed() {
		_cards_owed -= 1;
	}
	
	void take_cards(Player p) {
		try {_slap_pass.acquire(_numPlayers);
		} catch (InterruptedException e) {
            System.out.println("Woops!");
        }   
		
		int N = _cardsPile.size();
		if ((N > 1 && _cardsPile.get(N-1).substring(0, _cardsPile.get(N-1).length() -1).equals( _cardsPile.get(N-2).substring(0, _cardsPile.get(N-2).length() -1))) || (N > 2 && _cardsPile.get(N-1).substring(0, _cardsPile.get(N-1).length() -1).equals( _cardsPile.get(N-3).substring(0, _cardsPile.get(N-3).length() -1))) || (_face_case && p.get_id() == _face_id && _cards_owed == 0)){
			if ((N > 1 && _cardsPile.get(N-1).substring(0, _cardsPile.get(N-1).length() -1).equals( _cardsPile.get(N-2).substring(0, _cardsPile.get(N-2).length() -1))) || (N > 2 && _cardsPile.get(N-1).substring(0, _cardsPile.get(N-1).length() -1).equals( _cardsPile.get(N-3).substring(0, _cardsPile.get(N-3).length() -1))) ){
				System.out.println("Slap!");
			}
			for(String elt : _cardsPile){
				p.add_card(elt);
			}
			//Simulation.tryToSleep(5, 0); // <-- This is how we break it for lab 5
			_cardsPile.clear();
			System.out.println("Player " + p.get_id() + " has won the pile!");
			_turnID = p.get_id();
			if (_face_case){
				end_face_case();
			}
		} else {
			System.out.println("Woops! " + "Player " + p.get_id() +  " was too slow!");
		}
		
		_slap_pass.release(_numPlayers);
	}

	void place_card(Player p, String elt, boolean cards_left) {
		try {_slap_pass.acquire(_numPlayers);
		} catch (InterruptedException e) {
            System.out.println("Woops!");
        }   
		if (! cards_left){
			this.next_player();
			_consecutive_passes += 1;
		} else {
			System.out.println("Player " + p.get_id() + " put down the " + elt);
			_cardsPile.add(elt);
			_consecutive_passes = 0;
			_cards_owed -= 1;
			if (elt.contains("A")) {
				_cards_owed = 4;
				_face_case = true;
				_face_id = p.get_id();
				this.next_player();
			} else if (elt.contains("K")) {
				_cards_owed = 3;
				_face_case = true;
				_face_id = p.get_id();
				this.next_player();
			} else if (elt.contains("Q")) {
				_cards_owed = 2;
				_face_case = true;
				_face_id = p.get_id();
				this.next_player();
			} else if (elt.contains("J")) {
				_cards_owed = 1;
				_face_case = true;
				_face_id = p.get_id();
				this.next_player();
			} else if ((_cards_owed == 0) && ! _face_case){
				_cards_owed = 1;
				this.next_player();
			} else if(_cards_owed < 0) {
				System.out.println("Uh oh! That's weird... we shouldn't owe " + _cards_owed + "cards..."  );
				_cards_owed = 1;
				this.next_player();
			}
		}
		if(_consecutive_passes +1 == _numPlayers && _cardsPile.size() == 0){
			_gameOver = true;
			int vic = p.get_id() + 1 % _numPlayers;
			System.out.println("Wow! Player " + "Player " + vic  + " has won the game!" );
		}

	_slap_pass.release(_numPlayers);
	}
}
