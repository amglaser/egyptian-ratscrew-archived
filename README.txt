The name of the game is Egyptian Rat Screw! The goal is to get all of the cards from all of the opponents. 

The rules are as follows:
--------------------------------------------------------------------
The Pack

A standard 52-card deck is used and can include Jokers.
The Deal

Deal cards one at a time face down, to each player until all the cards have been dealt evenly. Without looking at any of the cards, each player squares up his hand into a neat pile in front of him.
The Play

Starting to the left of the dealer players pull the top card off their pile and place it face-up in the middle. If the card played is a number card, the next player puts down a card, too. This continues around the table until somebody puts down a face card or an Ace (J, Q, K, or A).

When a face card or an ace is played, the next person in the sequence must play another face card or an ace in order for play to continue.

If the next person in the sequence does not play a face card or an ace within their allotted chance, the person who played the last face card or an ace wins the round and the whole pile goes to them. The winner begins the next round of play.
When an Ace/King/Queen/Jack is placed down then next player must put down four/three/two/one cards (respecitively). Unless someone performs a valid slap or another face card is played, the original player takes the pile.

The only thing that overrides the face card or an ace rule is the slap rule. The first person to slap the pile of cards when the slap rule is put into effect is the winner of that round. If it cannot be determined who was the first to slap the pile, the person with the most fingers on top wins.
Slap Rules

Double - When two cards of equivalent value are laid down consecutively. Ex: 5, 5
Sandwich - When two cards of equivalent value are laid down consecutively, but with one card of different value between them. Ex: 5, 7, 5



You must add one or two cards to the bottom of the pile if you slap the pile when it was not slappable. (We will probably ignore this, as I don't think I will program players to make mistakes...)

Continue playing even if you have run out of cards. As long as you don't slap at the wrong time, you are still allowed to "slap in" and get cards! Everyone should try to stay in the game until you have a single winner who obtains all the cards

The player, who has all of the cards at the end of the game, wins.
-----------------------------------------------------------------------------------
(Rules obtained from: http://www.bicyclecards.com/how-to-play/egyptian-rat-screw/#sthash.t4opVP6n.dpuf)
(Although we're not using all of their rules, because some aren't standard use. Also I had to add a rule.)




There will be two classes, players and the (deck) pile. 
Each player will be an individual thread, and all threads will refer to the pile.
Interacting with the pile (by slapping) is where potential data races will arise.
The players have their id's, and the cards in their hand, and accept the pile as an object.
Players constantly check for opportunities to slap the pile, and will place a card into the pile when requested by the pile.
The pile will be responsible for determining whose turn it is, and how many cards they owe. 
The pile and players will like need functions to report the status of all of their variables to one another. (e.g. whose turn is it? how many cards are owed? are we in a face card condition?)
Messages of who slapped/won the pile will constitute the main functionality of our program.
Additionally, once the game is completed a winner will be declared and the number of cards in his hand will be checked to make sure the player wasn't "cheating" (i.e. extra cards were picked up due to a race condition). 